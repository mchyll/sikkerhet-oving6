#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>
#include <sys/wait.h>
#include "crypto.hpp"

using namespace std;

string real_hash = "ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6";
int key_len_bits = (int) (real_hash.length() * 4);

void permute(const int& length, const vector<string>& alphabet, const string& prefix = "") {
    if (length == 0) {
        string hash = Crypto::hex(Crypto::pbkdf2(prefix, "Saltet til Ola", 2048, key_len_bits));
        if (hash == real_hash) {
            cout << "Passordet er: " << prefix << endl;
            exit(0);
        }
    }
    else {
        for (int i = 0; i < alphabet.size(); i++) {
            permute(length - 1, alphabet, prefix + alphabet.at(i));
        }
    }
}

int main() {
    vector<string> alphabet = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
                               "s", "t", "u", "v", "w", "x", "y", "z", "æ", "ø", "å", "A", "B", "C", "D", "E", "F", "G",
                               "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y",
                               "Z", "Æ", "Ø", "Å", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"};

    for (int i = 1; i <= 4; i++) {
        cout << "Prøver passord med lengde " << i << endl;
        permute(i, alphabet);
    }
}
